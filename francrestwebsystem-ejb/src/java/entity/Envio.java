/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alejandro
 */
@Entity
@Table(name = "envio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Envio.findAll", query = "SELECT e FROM Envio e"),
    @NamedQuery(name = "Envio.findByIdEnvio", query = "SELECT e FROM Envio e WHERE e.idEnvio = :idEnvio"),
    @NamedQuery(name = "Envio.findByCodigoSeguridad", query = "SELECT e FROM Envio e WHERE e.codigoSeguridad = :codigoSeguridad"),
    @NamedQuery(name = "Envio.findByFechaRecogida", query = "SELECT e FROM Envio e WHERE e.fechaRecogida = :fechaRecogida"),
    @NamedQuery(name = "Envio.findByFechaEntrega", query = "SELECT e FROM Envio e WHERE e.fechaEntrega = :fechaEntrega")})
public class Envio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idEnvio")
    private Integer idEnvio;
    @Column(name = "codigoSeguridad")
    private Integer codigoSeguridad;
    @Column(name = "fechaRecogida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecogida;
    @Column(name = "fechaEntrega")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEntrega;
    @JoinColumn(name = "Cliente_DNICliente", referencedColumnName = "DNICliente")
    @ManyToOne(optional = false)
    private Cliente clienteDNICliente;
    @JoinColumn(name = "Transportista_idTransportista", referencedColumnName = "idTransportista")
    @ManyToOne(optional = false)
    private Transportista transportistaidTransportista;
    @JoinColumn(name = "EstadoEnvio_idEstadoEnvio", referencedColumnName = "idEstadoEnvio")
    @ManyToOne(optional = false)
    private Estadoenvio estadoEnvioidEstadoEnvio;
    @JoinColumn(name = "velocidadEntrega_idVelocidadEntrega", referencedColumnName = "idVelocidadEntrega")
    @ManyToOne(optional = false)
    private Velocidadentrega velocidadEntregaidVelocidadEntrega;
    @JoinColumn(name = "Producto_idProducto", referencedColumnName = "idProducto")
    @ManyToOne(optional = false)
    private Producto productoidProducto;
    @JoinColumn(name = "Direccion_idDireccion", referencedColumnName = "idDireccion")
    @ManyToOne(optional = false)
    private Direccion direccionidDireccion;

    public Envio() {
    }

    public Envio(Integer idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Integer getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Integer idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Integer getCodigoSeguridad() {
        return codigoSeguridad;
    }

    public void setCodigoSeguridad(Integer codigoSeguridad) {
        this.codigoSeguridad = codigoSeguridad;
    }

    public Date getFechaRecogida() {
        return fechaRecogida;
    }

    public void setFechaRecogida(Date fechaRecogida) {
        this.fechaRecogida = fechaRecogida;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Cliente getClienteDNICliente() {
        return clienteDNICliente;
    }

    public void setClienteDNICliente(Cliente clienteDNICliente) {
        this.clienteDNICliente = clienteDNICliente;
    }

    public Transportista getTransportistaidTransportista() {
        return transportistaidTransportista;
    }

    public void setTransportistaidTransportista(Transportista transportistaidTransportista) {
        this.transportistaidTransportista = transportistaidTransportista;
    }

    public Estadoenvio getEstadoEnvioidEstadoEnvio() {
        return estadoEnvioidEstadoEnvio;
    }

    public void setEstadoEnvioidEstadoEnvio(Estadoenvio estadoEnvioidEstadoEnvio) {
        this.estadoEnvioidEstadoEnvio = estadoEnvioidEstadoEnvio;
    }

    public Velocidadentrega getVelocidadEntregaidVelocidadEntrega() {
        return velocidadEntregaidVelocidadEntrega;
    }

    public void setVelocidadEntregaidVelocidadEntrega(Velocidadentrega velocidadEntregaidVelocidadEntrega) {
        this.velocidadEntregaidVelocidadEntrega = velocidadEntregaidVelocidadEntrega;
    }

    public Producto getProductoidProducto() {
        return productoidProducto;
    }

    public void setProductoidProducto(Producto productoidProducto) {
        this.productoidProducto = productoidProducto;
    }

    public Direccion getDireccionidDireccion() {
        return direccionidDireccion;
    }

    public void setDireccionidDireccion(Direccion direccionidDireccion) {
        this.direccionidDireccion = direccionidDireccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Envio)) {
            return false;
        }
        Envio other = (Envio) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Envio[ idEnvio=" + idEnvio + " ]";
    }
    
}
