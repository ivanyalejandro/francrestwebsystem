/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alejandro
 */
@Entity
@Table(name = "estadoenvio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estadoenvio.findAll", query = "SELECT e FROM Estadoenvio e"),
    @NamedQuery(name = "Estadoenvio.findByIdEstadoEnvio", query = "SELECT e FROM Estadoenvio e WHERE e.idEstadoEnvio = :idEstadoEnvio"),
    @NamedQuery(name = "Estadoenvio.findByNombreEstadoEnvio", query = "SELECT e FROM Estadoenvio e WHERE e.nombreEstadoEnvio = :nombreEstadoEnvio"),
    @NamedQuery(name = "Estadoenvio.findByDescripcionEstadoEnvio", query = "SELECT e FROM Estadoenvio e WHERE e.descripcionEstadoEnvio = :descripcionEstadoEnvio")})
public class Estadoenvio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idEstadoEnvio")
    private Integer idEstadoEnvio;
    @Size(max = 45)
    @Column(name = "nombreEstadoEnvio")
    private String nombreEstadoEnvio;
    @Size(max = 45)
    @Column(name = "descripcionEstadoEnvio")
    private String descripcionEstadoEnvio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "estadoEnvioidEstadoEnvio")
    private Collection<Envio> envioCollection;

    public Estadoenvio() {
    }

    public Estadoenvio(Integer idEstadoEnvio) {
        this.idEstadoEnvio = idEstadoEnvio;
    }

    public Integer getIdEstadoEnvio() {
        return idEstadoEnvio;
    }

    public void setIdEstadoEnvio(Integer idEstadoEnvio) {
        this.idEstadoEnvio = idEstadoEnvio;
    }

    public String getNombreEstadoEnvio() {
        return nombreEstadoEnvio;
    }

    public void setNombreEstadoEnvio(String nombreEstadoEnvio) {
        this.nombreEstadoEnvio = nombreEstadoEnvio;
    }

    public String getDescripcionEstadoEnvio() {
        return descripcionEstadoEnvio;
    }

    public void setDescripcionEstadoEnvio(String descripcionEstadoEnvio) {
        this.descripcionEstadoEnvio = descripcionEstadoEnvio;
    }

    @XmlTransient
    public Collection<Envio> getEnvioCollection() {
        return envioCollection;
    }

    public void setEnvioCollection(Collection<Envio> envioCollection) {
        this.envioCollection = envioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstadoEnvio != null ? idEstadoEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estadoenvio)) {
            return false;
        }
        Estadoenvio other = (Estadoenvio) object;
        if ((this.idEstadoEnvio == null && other.idEstadoEnvio != null) || (this.idEstadoEnvio != null && !this.idEstadoEnvio.equals(other.idEstadoEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Estadoenvio[ idEstadoEnvio=" + idEstadoEnvio + " ]";
    }
    
}
