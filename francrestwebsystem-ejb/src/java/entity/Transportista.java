/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alejandro
 */
@Entity
@Table(name = "transportista")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transportista.findAll", query = "SELECT t FROM Transportista t"),
    @NamedQuery(name = "Transportista.findByIdTransportista", query = "SELECT t FROM Transportista t WHERE t.idTransportista = :idTransportista"),
    @NamedQuery(name = "Transportista.findByNombreTransportista", query = "SELECT t FROM Transportista t WHERE t.nombreTransportista = :nombreTransportista"),
    @NamedQuery(name = "Transportista.findByApellidoTransportista", query = "SELECT t FROM Transportista t WHERE t.apellidoTransportista = :apellidoTransportista"),
    @NamedQuery(name = "Transportista.findByTelefonoTransportista", query = "SELECT t FROM Transportista t WHERE t.telefonoTransportista = :telefonoTransportista"),
    @NamedQuery(name = "Transportista.findByMailTransportista", query = "SELECT t FROM Transportista t WHERE t.mailTransportista = :mailTransportista"),
    @NamedQuery(name = "Transportista.findByDNITransportista", query = "SELECT t FROM Transportista t WHERE t.dNITransportista = :dNITransportista")})
public class Transportista implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idTransportista")
    private Integer idTransportista;
    @Size(max = 45)
    @Column(name = "nombreTransportista")
    private String nombreTransportista;
    @Size(max = 45)
    @Column(name = "apellidoTransportista")
    private String apellidoTransportista;
    @Size(max = 12)
    @Column(name = "telefonoTransportista")
    private String telefonoTransportista;
    @Size(max = 45)
    @Column(name = "mailTransportista")
    private String mailTransportista;
    @Size(max = 9)
    @Column(name = "DNITransportista")
    private String dNITransportista;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transportistaidTransportista")
    private Collection<Envio> envioCollection;

    public Transportista() {
    }

    public Transportista(Integer idTransportista) {
        this.idTransportista = idTransportista;
    }

    public Integer getIdTransportista() {
        return idTransportista;
    }

    public void setIdTransportista(Integer idTransportista) {
        this.idTransportista = idTransportista;
    }

    public String getNombreTransportista() {
        return nombreTransportista;
    }

    public void setNombreTransportista(String nombreTransportista) {
        this.nombreTransportista = nombreTransportista;
    }

    public String getApellidoTransportista() {
        return apellidoTransportista;
    }

    public void setApellidoTransportista(String apellidoTransportista) {
        this.apellidoTransportista = apellidoTransportista;
    }

    public String getTelefonoTransportista() {
        return telefonoTransportista;
    }

    public void setTelefonoTransportista(String telefonoTransportista) {
        this.telefonoTransportista = telefonoTransportista;
    }

    public String getMailTransportista() {
        return mailTransportista;
    }

    public void setMailTransportista(String mailTransportista) {
        this.mailTransportista = mailTransportista;
    }

    public String getDNITransportista() {
        return dNITransportista;
    }

    public void setDNITransportista(String dNITransportista) {
        this.dNITransportista = dNITransportista;
    }

    @XmlTransient
    public Collection<Envio> getEnvioCollection() {
        return envioCollection;
    }

    public void setEnvioCollection(Collection<Envio> envioCollection) {
        this.envioCollection = envioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTransportista != null ? idTransportista.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transportista)) {
            return false;
        }
        Transportista other = (Transportista) object;
        if ((this.idTransportista == null && other.idTransportista != null) || (this.idTransportista != null && !this.idTransportista.equals(other.idTransportista))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Transportista[ idTransportista=" + idTransportista + " ]";
    }
    
}
