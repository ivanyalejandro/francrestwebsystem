/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alejandro
 */
@Entity
@Table(name = "velocidadentrega")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Velocidadentrega.findAll", query = "SELECT v FROM Velocidadentrega v"),
    @NamedQuery(name = "Velocidadentrega.findByIdVelocidadEntrega", query = "SELECT v FROM Velocidadentrega v WHERE v.idVelocidadEntrega = :idVelocidadEntrega"),
    @NamedQuery(name = "Velocidadentrega.findByNombreVelocidadEntrega", query = "SELECT v FROM Velocidadentrega v WHERE v.nombreVelocidadEntrega = :nombreVelocidadEntrega"),
    @NamedQuery(name = "Velocidadentrega.findByDescripcionVelocidadEntrega", query = "SELECT v FROM Velocidadentrega v WHERE v.descripcionVelocidadEntrega = :descripcionVelocidadEntrega"),
    @NamedQuery(name = "Velocidadentrega.findByPrecioVelocidadEntrega", query = "SELECT v FROM Velocidadentrega v WHERE v.precioVelocidadEntrega = :precioVelocidadEntrega")})
public class Velocidadentrega implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idVelocidadEntrega")
    private Integer idVelocidadEntrega;
    @Size(max = 45)
    @Column(name = "nombreVelocidadEntrega")
    private String nombreVelocidadEntrega;
    @Size(max = 45)
    @Column(name = "descripcionVelocidadEntrega")
    private String descripcionVelocidadEntrega;
    @Size(max = 45)
    @Column(name = "precioVelocidadEntrega")
    private String precioVelocidadEntrega;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "velocidadEntregaidVelocidadEntrega")
    private Collection<Envio> envioCollection;

    public Velocidadentrega() {
    }

    public Velocidadentrega(Integer idVelocidadEntrega) {
        this.idVelocidadEntrega = idVelocidadEntrega;
    }

    public Integer getIdVelocidadEntrega() {
        return idVelocidadEntrega;
    }

    public void setIdVelocidadEntrega(Integer idVelocidadEntrega) {
        this.idVelocidadEntrega = idVelocidadEntrega;
    }

    public String getNombreVelocidadEntrega() {
        return nombreVelocidadEntrega;
    }

    public void setNombreVelocidadEntrega(String nombreVelocidadEntrega) {
        this.nombreVelocidadEntrega = nombreVelocidadEntrega;
    }

    public String getDescripcionVelocidadEntrega() {
        return descripcionVelocidadEntrega;
    }

    public void setDescripcionVelocidadEntrega(String descripcionVelocidadEntrega) {
        this.descripcionVelocidadEntrega = descripcionVelocidadEntrega;
    }

    public String getPrecioVelocidadEntrega() {
        return precioVelocidadEntrega;
    }

    public void setPrecioVelocidadEntrega(String precioVelocidadEntrega) {
        this.precioVelocidadEntrega = precioVelocidadEntrega;
    }

    @XmlTransient
    public Collection<Envio> getEnvioCollection() {
        return envioCollection;
    }

    public void setEnvioCollection(Collection<Envio> envioCollection) {
        this.envioCollection = envioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVelocidadEntrega != null ? idVelocidadEntrega.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Velocidadentrega)) {
            return false;
        }
        Velocidadentrega other = (Velocidadentrega) object;
        if ((this.idVelocidadEntrega == null && other.idVelocidadEntrega != null) || (this.idVelocidadEntrega != null && !this.idVelocidadEntrega.equals(other.idVelocidadEntrega))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Velocidadentrega[ idVelocidadEntrega=" + idVelocidadEntrega + " ]";
    }
    
}
