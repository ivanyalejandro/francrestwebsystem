/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alejandro
 */
@Entity
@Table(name = "cliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c"),
    @NamedQuery(name = "Cliente.findByDNICliente", query = "SELECT c FROM Cliente c WHERE c.dNICliente = :dNICliente"),
    @NamedQuery(name = "Cliente.findByNombreCliente", query = "SELECT c FROM Cliente c WHERE c.nombreCliente = :nombreCliente"),
    @NamedQuery(name = "Cliente.findByApellidosCliente", query = "SELECT c FROM Cliente c WHERE c.apellidosCliente = :apellidosCliente"),
    @NamedQuery(name = "Cliente.findByTelefonoCliente", query = "SELECT c FROM Cliente c WHERE c.telefonoCliente = :telefonoCliente"),
    @NamedQuery(name = "Cliente.findByMailCliente", query = "SELECT c FROM Cliente c WHERE c.mailCliente = :mailCliente")})
public class Cliente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "DNICliente")
    private String dNICliente;
    @Size(max = 45)
    @Column(name = "nombreCliente")
    private String nombreCliente;
    @Size(max = 45)
    @Column(name = "apellidosCliente")
    private String apellidosCliente;
    @Size(max = 12)
    @Column(name = "telefonoCliente")
    private String telefonoCliente;
    @Size(max = 45)
    @Column(name = "mailCliente")
    private String mailCliente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clienteDNICliente")
    private Collection<Envio> envioCollection;
    @JoinColumn(name = "Direccion_idDireccion", referencedColumnName = "idDireccion")
    @ManyToOne(optional = false)
    private Direccion direccionidDireccion;

    public Cliente() {
    }

    public Cliente(String dNICliente) {
        this.dNICliente = dNICliente;
    }

    public String getDNICliente() {
        return dNICliente;
    }

    public void setDNICliente(String dNICliente) {
        this.dNICliente = dNICliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getApellidosCliente() {
        return apellidosCliente;
    }

    public void setApellidosCliente(String apellidosCliente) {
        this.apellidosCliente = apellidosCliente;
    }

    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    public String getMailCliente() {
        return mailCliente;
    }

    public void setMailCliente(String mailCliente) {
        this.mailCliente = mailCliente;
    }

    @XmlTransient
    public Collection<Envio> getEnvioCollection() {
        return envioCollection;
    }

    public void setEnvioCollection(Collection<Envio> envioCollection) {
        this.envioCollection = envioCollection;
    }

    public Direccion getDireccionidDireccion() {
        return direccionidDireccion;
    }

    public void setDireccionidDireccion(Direccion direccionidDireccion) {
        this.direccionidDireccion = direccionidDireccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dNICliente != null ? dNICliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.dNICliente == null && other.dNICliente != null) || (this.dNICliente != null && !this.dNICliente.equals(other.dNICliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Cliente[ dNICliente=" + dNICliente + " ]";
    }
    
}
