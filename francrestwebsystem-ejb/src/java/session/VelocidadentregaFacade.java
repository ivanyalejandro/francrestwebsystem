/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package session;

import entity.Velocidadentrega;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alejandro
 */
@Stateless
public class VelocidadentregaFacade extends AbstractFacade<Velocidadentrega> {
    @PersistenceContext(unitName = "francrestwebsystem-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VelocidadentregaFacade() {
        super(Velocidadentrega.class);
    }
    
}
